echo $1;

cd $1;
cd frontend;
npm install;
npm run build;
cd ..;
cp -r frontend/build/* soundoff/soundoff/static/;
mkdir .elasticbeanstalk
cp ci/eb-config.yml .elasticbeanstalk/config.yml

eb deploy Soundoff-env
