import random
import base64
import boto3
import os
from botocore.exceptions import ClientError
import tempfile


class SoundUploader:
    def upload(self, base64_data):

        # This function creates a file in a temp directory to accommodate limitations on Windows where temporary files
        # cannot be opened more than once (which is needed for the upload method)
        tmp = tempfile.TemporaryFile()
        tmp_dir = os.path.dirname(tmp.name)

        hash = random.getrandbits(128)

        path = "%032x.mp3" % hash

        temp_path = tmp_dir + path
        f = open(temp_path, "wb")

        f.write(base64.standard_b64decode(base64_data))
        f.close()

        self.upload_file(self, temp_path, 'soundoff.sounds', path)

        os.remove(temp_path)

        return "https://s3.amazonaws.com/soundoff.sounds/" + path

    def upload_file(self, file_name, bucket, object_name):
        if object_name is None:
            object_name = os.path.basename(file_name)

        # Upload the file
        s3_client = boto3.client('s3')
        try:
            response = s3_client.upload_file(file_name, bucket, object_name, {
                'ContentType': 'audio/mpeg'
            })
        except ClientError as e:
            return False
        return True