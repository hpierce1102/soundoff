from django.http import HttpResponse
from django.core.serializers import serialize
from soundoff.soundoff.models import Sound
from django.views.decorators.csrf import csrf_exempt
import json
import re
import random
import os
import base64
import sys
from soundoff.soundoff.services import SoundUploader

def parse_media_type(soundData):
    media_type_regex = re.compile('data:(.*);')
    match = media_type_regex.search(soundData)

    return match.group(1)

def home(request):
    test_file = open('soundoff/soundoff/static/index.html', 'rb')
    response = HttpResponse(content=test_file)
    response['Content-Type'] = 'text/html'

    return response

@csrf_exempt
def list_sounds(request):
    if request.method == 'GET':
        response = HttpResponse(serialize('json', Sound.objects.all()))
        response.headers['Content-Type'] = 'application/json'

        return response
    elif request.method == 'POST':
        content = json.loads(request.body)

        sound_data = content['sound']

        if (len(sound_data) > 5000000):
            raise Exception('sound must be less than 5 MB')

        allowed_types = ['audio/mpeg']
        if (parse_media_type(sound_data) not in allowed_types):
            raise Exception('File must be an allowed type: ' . join(allowed_types))

        sound_service = SoundUploader

        url = sound_service.upload(sound_service, sound_data)

        sound = Sound()
        sound.url = url
        sound.save()

        sounds = serialize('json', [sound])
        single = json.loads(sounds)[0]

        response = HttpResponse(json.dumps(single))
        response.headers['Content-Type'] = 'application/json'

        return response
    else:
        response = HttpResponse(status=405)
        return response
