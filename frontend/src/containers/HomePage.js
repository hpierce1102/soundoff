import {useEffect, useState} from 'react';
import Sound from "../components/Sound";
import '../fonts/Lobster_1.3.otf';
import './HomePage.css';
import Modal from "../components/Modal";
import AddSoundFormContainer from "./AddSoundFormContainer";

function HomePage() {
    const [sounds, setSounds] = useState([]);
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [fetchSoundsCounter, setFetchSoundsCounter] = useState(0)

    useEffect(() => {
        fetch(new Request('/sounds'))
            .then(content => content.blob())
            .then(blob => blob.text())
            .then(text => {
                setSounds(JSON.parse(text))
            })
            .catch(error => console.error(error));
    }, [fetchSoundsCounter]);

    return (
        <div className="homepage__container">
            <Modal
                visible={isModalOpen}
                onClose={() => setIsModalOpen(false)}
                title="Add a Sound"
            >
                <AddSoundFormContainer
                    onComplete={() => {
                        setIsModalOpen(false)
                        setFetchSoundsCounter(fetchSoundsCounter + 1)
                    }}
                />
            </Modal>
            <div className="homepage__content">
                <div className="homepage__headerContainer">
                    <h1>Sound Off</h1>
                </div>

                <div className="homepage__soundGrid">
                    {sounds.map((sound) => (
                        <div key={sound.pk} className="homepage__soundContainer">
                            <Sound
                                url={sound.fields.url}
                            />
                        </div>
                    ))}
                </div>
                <button onClick={() => {setIsModalOpen(true)}}>Open Modal</button>
            </div>
        </div>
    )
}

export default HomePage;
