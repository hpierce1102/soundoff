import './Modal.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { solid } from '@fortawesome/fontawesome-svg-core/import.macro'

function Modal({visible, onClose, title, children}) {
    return (
        <>
            {visible && (
                <div className="modal__background">
                    <div className="modal__container">
                        <div className="modal__header">
                            <h3 className="modal__headerTitle">{title}</h3>
                            <div className="modal__headerXIconContainer" onClick={() => { onClose() }}>
                                <div className="modal__headerXIcon">
                                    <FontAwesomeIcon size="lg" color="white" icon={solid('xmark')} />
                                </div>
                            </div>
                        </div>
                        <div className="clearfix"/>
                        <div className="modal__body">
                            {children}
                        </div>
                    </div>
                </div>
            )}
        </>

    )
}

export default Modal