import { useState} from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { solid } from '@fortawesome/fontawesome-svg-core/import.macro'
import "./Sound.css";
import AudioInner from "./AudioInner";

function Sound({url}) {
    const [isPlaying, setIsPlaying] = useState(false);

    return (
        <div className="sound__container">
            <AudioInner
                audioUrl={url}
                onPlay={() => {setIsPlaying(true)}}
                onStop={() => {setIsPlaying(false)}}
            >
                {({playSound, stopSound}) => (
                    <div
                        className="sound__iconContainer"
                        onClick={() => { isPlaying ? stopSound() : playSound() }}
                    >
                        {isPlaying ? (
                            <FontAwesomeIcon size="3x" color="white" icon={solid('stop')} />
                        ) : (
                            <FontAwesomeIcon size="3x" color="white" icon={solid('play')} />
                        )}

                    </div>
                )}
            </AudioInner>
        </div>
    )
}

export default Sound;