import {useCallback, useRef} from "react";


function AudioInner({audioUrl, onPlay, onStop, children}) {
    const audioRef = useRef(new Audio(audioUrl));

    audioRef.current.addEventListener('play', () => {
        onPlay();
    });

    audioRef.current.addEventListener('pause', () => {
        onStop();
    });

    const playSound = useCallback(() => {
        audioRef.current.play();
    }, [audioRef]);

    const stopSound = useCallback(() => {
        audioRef.current.pause();
        audioRef.current.currentTime = 0;
    }, [audioRef]);

    return (
        <>
            {children({
                playSound,
                stopSound
            })}
        </>
    );
}

export default AudioInner;