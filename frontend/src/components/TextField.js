import './TextField.css';

function TextField({name, onChange, onBlur, value}) {
    return (
        <div className="textField__container">
            <input
                className="textField__containerInput"
                type="text"
                name={name}
                onChange={onChange}
                onBlur={onBlur}
                value={value}
            />
        </div>
    )
}

export default TextField;