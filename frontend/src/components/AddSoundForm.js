import {useState} from 'react';
import {Formik} from "formik";
import './AddSoundForm.css';

function AddSoundForm({onSubmit}) {
    const [file, setFile] = useState();

    return (
        <Formik
            initialValues={{
                url: '',
                file: '',
            }}
            onSubmit={(data) => {
                onSubmit({...data, fileContents: file});
            }}
        >
            {({
                values,
                handleChange,
                handleBlur,
                handleSubmit,
                setFieldValue
            }) => (
                <form onSubmit={handleSubmit}>
                    <input
                        type="file"
                        name="file"
                        onChange={(e) => {
                          const fileReader = new FileReader();
                          fileReader.onload = () => {
                            if (fileReader.readyState === 2) {
                              console.log(fileReader.result)
                              setFile(fileReader.result);
                            }
                          };
                          fileReader.readAsDataURL(e.target.files[0]);

                          handleChange(e);
                        }}
                        onBlur={handleBlur}
                        value={values.file}
                    />

                   <button type="submit">Add</button>
                </form>
            )}
        </Formik>
    )
}

export default AddSoundForm;