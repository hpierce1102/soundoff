FROM ubuntu

# Install EB CLI
RUN apt-get update
RUN apt-get install -y python3 python3-pip python-is-python3 git
RUN pip install virtualenv
RUN git clone https://github.com/aws/aws-elastic-beanstalk-cli-setup.git
RUN python ./aws-elastic-beanstalk-cli-setup/scripts/ebcli_installer.py
ENV PATH="/root/.ebcli-virtual-env/executables:${PATH}"

# Install node
RUN apt-get install -y curl
RUN curl -sL https://deb.nodesource.com/setup_18.x | bash -
RUN apt-get install -y nodejs

WORKDIR /home
CMD ["bash"]
